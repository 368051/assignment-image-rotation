//
// Created by liza on 27.10.2023.
//
#ifndef IMAGE_TRANSFORMER_FILE_IO_H
#define IMAGE_TRANSFORMER_FILE_IO_H

#include "models.h"
#include <stdio.h>

void close_file(FILE *file);

int read_file(const char *input_file_path, struct BMPInfo *info, struct image *image);

int write_in_file(const char *output_file_path, struct BMPInfo *info, struct image *image);

#endif //IMAGE_TRANSFORMER_FILE_IO_H
