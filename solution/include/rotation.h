//
// Created by liza on 27.10.2023.
//
#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

#include "models.h"

int rotate(struct BMPInfo *info, struct BMPInfo *infoNew, struct image *image, struct image *imageNew);

#endif //IMAGE_TRANSFORMER_ROTATION_H
