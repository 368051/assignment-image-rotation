//
// Created by liza on 26.10.2023.
//
#ifndef IMAGE_TRANSFORMER_FUNCS_H
#define IMAGE_TRANSFORMER_FUNCS_H

#include "models.h"
#include <stdio.h>

FILE *open_file(const char *path, const char *mode);

void fill_BMPInfo(struct BMPInfo *info, FILE *file);

void fill_image(struct BMPInfo *info, struct image *image);

long count_padding(uint64_t width);

void fill_pixels(FILE *file, struct image *image, long padding);

void fill_file_with_BMPInfo(struct BMPInfo *info, FILE *file);

void fill_file_with_pixels(struct image *image, FILE *file, long padding);

void free_img(struct image *image);

#endif //IMAGE_TRANSFORMER_FUNCS_H
