//
// Created by liza on 27.10.2023.
//
#include "../include/funcs.h"
#include <stdio.h>
#include <stdlib.h>

// Close file
void close_file(FILE *file) {
    if (file != NULL) {
        fclose(file);
    }
}

//Open file and fill BMPInfo, image
int read_file(const char *input_path, struct BMPInfo *info, struct image *image) {
    FILE *file = open_file(input_path, "rb");
    if (!file) {
        return 1;
    }
    fill_BMPInfo(info, file);
    fill_image(info, image);
    long padding = count_padding(image->width);
    fill_pixels(file, image, padding);
    close_file(file);
    return 0;
}

//Open file and fill it with BMPInfo, image
int write_in_file(const char *output_path, struct BMPInfo *info, struct image *image) {
    FILE *file = open_file(output_path, "wb");
    if (!file) {
        return 1;
    }
    fill_file_with_BMPInfo(info, file);
    long padding = count_padding(image->width);
    fill_file_with_pixels(image, file, padding);
    close_file(file);
    free(image->data);
    return 0;
}
