//
// Created by liza on 26.10.2023.
//
#include "../include/models.h"
#include <stdlib.h>

#define PIXEL_SIZE sizeof(struct pixel)

//Switch BMPInfo
void copyAndUpdateBMPInfo(struct BMPInfo *info, struct BMPInfo *infoNew, struct image *image) {
    *infoNew = *info;
    infoNew->width = image->width;
    infoNew->height = image->height;
}

//Swap data about height and width of image
void swapImageDimensions(struct image *image, struct image *imageNew) {
    imageNew->width = image->height;
    imageNew->height = image->width;
}

//Get rotated index of pixel in image
size_t get_rotated_index(size_t h, size_t w, size_t width, size_t height) {
    return (height - 1 - w) * width + h;
}

//Rotate pixels
void rotatePixels(struct image *image, struct image *imageNew) {
    imageNew->data = (struct pixel *) malloc(imageNew->height * imageNew->width * PIXEL_SIZE);
    if (imageNew->data == NULL) {
        exit(EXIT_FAILURE);
    }
    for (size_t h = 0; h < imageNew->height; h++) {
        for (size_t w = 0; w < imageNew->width; w++) {
            imageNew->data[h * imageNew->width + w] = image->data[get_rotated_index(h, w, image->width, image->height)];
        }
    }
}

//Rotate image
int rotate(struct BMPInfo *info, struct BMPInfo *infoNew, struct image *image, struct image *imageNew) {
    swapImageDimensions(image, imageNew);
    rotatePixels(image, imageNew);
    copyAndUpdateBMPInfo(info, infoNew, imageNew);
    free(image->data);
    return 0;
}
