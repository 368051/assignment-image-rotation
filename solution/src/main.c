#include "../include/file_io.h"
#include "../include/funcs.h"
#include "../include/rotation.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        return 1;
    }

    const char *inputPath = argv[1];
    const char *outputPath = argv[2];

    struct BMPInfo info;
    struct image image;
    struct image imageNew;
    struct BMPInfo infoNew;

    if (read_file(inputPath, &info, &image) != 0) {
        fprintf(stderr, "Error reading input file: %s\n", inputPath);
        free_img(&image);
        return 1;
    }

    if (rotate(&info, &infoNew, &image, &imageNew) != 0) {
        fprintf(stderr, "Error rotating image\n");
        free_img(&image);
        free_img(&imageNew);
        return 1;
    }

    if (write_in_file(outputPath, &infoNew, &imageNew) != 0) {
        fprintf(stderr, "Error writing output file: %s\n", outputPath);
        free_img(&image);
        free_img(&imageNew);
        return 1;
    }

    return 0;
}
