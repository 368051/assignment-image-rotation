//
// Created by liza on 26.10.2023.
//
#include "../include/models.h"
#include <stdio.h>
#include <stdlib.h>

#define BMP_INFO_SIZE sizeof(struct BMPInfo)
#define PIXEL_SIZE sizeof(struct pixel)
#define BYTE_ALIGNMENT 4

//Open file from path with any mode
FILE *open_file(const char *path, const char *mode) {
    FILE *file = fopen(path, mode);
    if (!file) {
        return 0;
    }
    return file;
}

// Read data from file and put into BMPInfo
void fill_BMPInfo(struct BMPInfo *info, FILE *file) {
    size_t elements_read = fread(info, BMP_INFO_SIZE, 1, file);
    if (elements_read != 1) {
        exit(EXIT_FAILURE);
    }
}

//Fill an image info with data from BMPInfo and allocate memory for image data
void fill_image(struct BMPInfo *info, struct image *image) {
    image->height = info->height;
    image->width = info->width;
    image->data = malloc(image->height * image->width * PIXEL_SIZE);
    if (image->data == NULL) {
        exit(EXIT_FAILURE);
    }
}

//Count a padding of image
long count_padding(uint64_t width) {
    uint64_t bytes_in_row = width * PIXEL_SIZE;
    return (long) (BYTE_ALIGNMENT - (bytes_in_row % BYTE_ALIGNMENT)) % BYTE_ALIGNMENT;
}

//Fill an image data with data from file taking into account the padding
void fill_pixels(FILE *file, struct image *image, long padding) {
    for (size_t h = 0; h <= image->height - 1; h++) {
        fread(&(image->data[h * image->width]), PIXEL_SIZE, image->width, file);
        fseek(file, padding, SEEK_CUR);
    }
}

//Fill a file with BMPInfo
void fill_file_with_BMPInfo(struct BMPInfo *info, FILE *file) {
    size_t elements_written = fwrite(info, BMP_INFO_SIZE, 1, file);
    if (elements_written != 1) {
        exit(EXIT_FAILURE);
    }
}


//Fill a file with pixels
void fill_file_with_pixels(struct image *image, FILE *file, long padding) {
    for (size_t h = 0; h < image->height; h++) {
        size_t elements_written = fwrite(&image->data[h * image->width], PIXEL_SIZE, image->width, file);
        if (elements_written != image->width) {
            exit(EXIT_FAILURE);
        }
        uint8_t paddingBytes[4] = {0};
        size_t padding_written = fwrite(paddingBytes, 1, padding, file);
        if (padding_written != padding) {
            exit(EXIT_FAILURE);
        }
    }
}

//Free memory
void free_img(struct image *image) {
    free(image->data);
}
